class CreateRobots < ActiveRecord::Migration[6.0]
  def change
    create_table :robots do |t|
      t.string :name
      t.string :slogan
      t.boolean :active_status
      t.string :weapon

      t.timestamps
    end
  end
end

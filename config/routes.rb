Rails.application.routes.draw do
  resources :weapons
  get 'robot/index'
  get 'robot/new'
  get 'robot/edit'
  get 'robot/show'
  resources :robots
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
